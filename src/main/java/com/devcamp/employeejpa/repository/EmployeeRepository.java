package com.devcamp.employeejpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.employeejpa.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{
    
}
